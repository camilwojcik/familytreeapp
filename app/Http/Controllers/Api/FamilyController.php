<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Family;
use App\Models\FamilyMember;
use Illuminate\Support\Facades\DB;


class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Family::orderBy('created_at', 'asc')->get();  //returns values in ascending order
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //required fields include both for family data and family`s  1st member !
        $data = $request->validate([
            'family_name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'sex' => 'required'
        ]);

        $family = new Family;
        $family->family_name = $request->input('family_name');
        // $family->user_id = $request->input('family_name');
        $family->description = $request->input('description');
        $family->save(); //storing values as an object

        $familyMember = new FamilyMember;
        $familyMember->first_name = $request->input('first_name');
        $familyMember->last_name = $request->input('last_name');
        $familyMember->sex = $request->input('sex');
        $familyMember->date_of_birth = $request->input('date_of_birth') ?? null;
        $familyMember->family_id = $family->id;
        $familyMember->save();
        return $family; //returns the stored value if the operation was successful.
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Family::findorFail($id); //searches for the object in the database using its id and returns it.
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [ // the new values should not be null
            'family_name' => 'required',
        ]);

        $family = Family::findorFail($id); // uses the id to search values that need to be updated.
        $family->family_name = $request->input('family_name'); //retrieves user input
        $family->description = $request->input('description') ?? null;////retrieves user input
        $family->save();//saves the values in the database. The existing data is overwritten.

        return $family; // retrieves the updated object from the database
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $family = Family::findorFail($id); //searching for object in database using ID
        if($family->delete()){ //deletes the object
            return 'deleted successfully'; //shows a message when the delete operation was successful.
        }
    }

    /**
     * Get youngest ancestor of family->users_id
     * @param  int  $family - family_id
     * @return \Illuminate\Http\Response
     */
    public function youngest($family)
    {
        $result = DB::select('
            WITH recursive cte AS (
                SELECT fm.*
                FROM family_members AS fm
                WHERE fm.family_id =?
                UNION ALL
                SELECT p.*
                FROM family_members AS p
                INNER JOIN cte ON (p.parent_id = cte.id)
                WHERE p.family_id=?
            )
            SELECT * FROM cte ORDER BY IFNULL(cte.date_of_birth, NOW()) DESC LIMIT 1
        ', [$family, $family]);

        return $result;
    }
}
