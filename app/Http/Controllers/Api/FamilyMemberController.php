<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FamilyMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FamilyMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return FamilyMember::orderBy('created_at', 'asc')->get();  //returns values in ascending order
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $member)
    {
        $this->validate($request, [ // the new values should not be null
            'first_name' => 'required',
        ]);

        $familyMember = new FamilyMember;
        $familyMember->first_name = $request->input('first_name');
        $familyMember->last_name = $request->input('last_name') ?? null;
        $familyMember->sex = $request->input('sex');
        $familyMember->date_of_birth = $request->input('date_of_birth') ?? null;
        $familyMember->date_of_death = $request->input('date_of_death') ?? null;
        $familyMember->family_id = $request->input('family_id');
        $familyMember->parent_id = $member;
        $familyMember->relationship = $request->input('relationship');

        if($request->input('relationship') === 'parent'){
                //if we`re adding a parent to highest member out parent_id is the node we are coming from 
                //(means that we need to insert new with null value of parent_id and update exsiting node of highest parent with child relationship)
                $familyMember->parent_id = null;
                $familyMemberChild = DB::table('family_members')
                    ->where('family_id', $request->input('family_id'))
                    ->where('id', $member)
                ;
                $familyMember->save();
                $familyMemberChild->update(['parent_id' => $member, 'relationship' => $request->input('relationship')]);

                return $familyMember;
        }
        $familyMember->save();

        return $familyMember; //returns the stored value if the operation was successful.
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  int  $member
     * @return \Illuminate\Http\Response
     */
    public function show($family, $member)
    {
        $result = DB::select('
            WITH recursive cte AS (
                SELECT fm.*
                FROM family_members AS fm
                WHERE fm.family_id =? AND fm.id=?
                UNION ALL
                SELECT p.*
                FROM family_members AS p
                INNER JOIN cte ON (p.parent_id = cte.id)
                WHERE p.family_id=?
            )
            SELECT * FROM cte 
        ', [$family, $member, $family]);

        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [ // the new values should not be null
            'first_name' => 'required',
            'sex' => 'required',
        ]);

        $familyMember = FamilyMember::findorFail($id);
        $familyMember->first_name = $request->input('first_name');
        $familyMember->last_name = $request->input('last_name') ?? null;
        $familyMember->sex = $request->input('last_name');
        $familyMember->date_of_birth = $request->input('date_of_birth') ?? null;
        $familyMember->date_of_death = $request->input('date_of_death') ?? null;
        $familyMember->family_id = $familyMember->family_id; //this cannot be changed
        $familyMember->parent_id = $request->input('parent_id');
        $familyMember->save();
        return $familyMember;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $familyMember = FamilyMember::findorFail($id); //searching for object in database using ID
        if($familyMember->delete()){ //deletes the object
            return 'deleted successfully'; //shows a message when the delete operation was successful.
        }
    }

    /**
     * Get youngest ancestor of family/member_id
     * @param  int  $family - family_id
     * @param  int  $member - member_id
     * @return \Illuminate\Http\Response
     */
    public function youngest($family, $member)
    {
        $result = DB::select('
            WITH recursive cte AS (
                SELECT fm.*
                FROM family_members AS fm
                WHERE fm.family_id =? AND fm.id=?
                UNION ALL
                SELECT p.*
                FROM family_members AS p
                INNER JOIN cte ON (p.parent_id = cte.id)
                WHERE p.family_id=?
            )
            SELECT * FROM cte ORDER BY IFNULL(cte.date_of_birth, NOW()) DESC LIMIT 1
        ', [$family, $member, $family]);

        return $result;
    }
}
