<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('family', \App\Http\Controllers\Api\FamilyController::class);
Route::apiResource('family.member', \App\Http\Controllers\Api\FamilyMemberController::class);

// Awaits for API auth
// Route::apiResource('family', \App\Http\Controllers\Api\FamilyController::class)->middleware('auth');
// Route::apiResource('family.member', \App\Http\Controllers\Api\FamilyMemberController::class)->middleware('auth');
Route::get('/family/{family}/youngest', "\App\Http\Controllers\Api\FamilyController@youngest",)->name('family.youngest');
Route::get('/family/{family}/member/{member}/youngest', "\App\Http\Controllers\Api\FamilyMemberController@youngest",)->name('family.member.youngest');
