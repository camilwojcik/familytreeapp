<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //sometimes migrating down wasn`t properly removing this table, as a 1st migration it`s fine
        Schema::dropIfExists('families');

        Schema::create('families', function (Blueprint $table) {
            $table->increments('id');
            // $table->bigInteger('user_id')->unsigned();
            $table->string('family_name');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        //removing this part untill i`ll fix logging in and API authentication
        // Schema::table('families', function (Blueprint $table) {
        //     $table->foreign('user_id')->references('id')->on('users');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
};
