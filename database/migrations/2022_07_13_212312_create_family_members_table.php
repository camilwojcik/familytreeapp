<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->boolean('sex');
            $table->date('date_of_birth')->nullable();
            $table->date('date_of_death')->nullable();
            $table->integer('family_id')->unsigned();
            $table->integer('parent_id')->nullable()->unsigned();
            $table->enum('relationship', ['partner', 'child', 'parent'])->nullable();
            $table->timestamps();
        });

        Schema::table('family_members', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('family_members');
            $table->foreign('family_id')->references('id')->on('families');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_members');
    }
};
