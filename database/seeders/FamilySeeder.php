<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FamilySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $family_id = DB::table('families')->insertGetId([
            'id' => 1,
            'family_name' => 'Wojcik',
            'description' => 'TestFamily'
        ]);
        DB::table('family_members')->insert([
            'first_name' => 'krzysztof',
            'last_name' => 'wojcik',
            'sex' => 1,
            'date_of_birth' => '2022-07-14',
            'date_of_death' => NULL,
            'family_id' => $family_id,
            'parent_id' => NULL,
            'relationship' => NULL
        ]);
        DB::table('family_members')->insert([
            'first_name' => 'magdalena',
            'last_name' => 'wojcik',
            'sex' => 0,
            'date_of_birth' => '2022-07-13',
            'date_of_death' => NULL,
            'family_id' => $family_id,
            'parent_id' => 1,
            'relationship' => 'partner'
        ]);
        DB::table('family_members')->insert([
            'first_name' => 'kamil',
            'last_name' => 'wojcik',
            'sex' => 1,
            'date_of_birth' => '2022-07-12',
            'date_of_death' => NULL,
            'family_id' => $family_id,
            'parent_id' => 1,
            'relationship' => 'child'
        ]);
        DB::table('family_members')->insert([
            'first_name' => 'imaginary_gf',
            'last_name' => 'wojcik',
            'sex' => 0,
            'date_of_birth' => '2022-07-12',
            'date_of_death' => NULL,
            'family_id' => $family_id,
            'parent_id' => 3,
            'relationship' => 'partner'
        ]);
        DB::table('family_members')->insert([
            'first_name' => 'imaginary_son',
            'last_name' => 'wojcik',
            'sex' => 1,
            'date_of_birth' => '2022-07-12',
            'date_of_death' => NULL,
            'family_id' => $family_id,
            'parent_id' => 3,
            'relationship' => 'child'
        ]);
        DB::table('family_members')->insert([
            'first_name' => 'Mateusz',
            'last_name' => 'wojcik',
            'sex' => 1,
            'date_of_birth' => '2022-07-12',
            'date_of_death' => NULL,
            'family_id' => $family_id,
            'parent_id' => 1,
            'relationship' => 'child'
        ]);
    }
}
